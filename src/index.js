import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import one from './knight-before.png'
import two from './knight-after.png'
import pot from './pot.png'
import coin from './coin.png'


export default class App extends Component {
	constructor(){
		super()
		this.state = {
			showState: true,
			hp:0,
			startHP:0, // записываем начальное значение здоровья
			damage: 10,
			beforeDamage: 0,
			monsterdamage:0, 
	      	monster: [
	        {
		        name:"Women Knight",
		        hp:10000,
		        id:11111,
		        damageAdd:15
	        }
	      ]
		}
		this.goldBeforeDamage = this.goldBeforeDamage.bind(this)
		this.resetHp 		  = this.resetHp.bind(this)
		this.onChangeHp 	  = this.onChangeHp.bind(this)
		this.upperHp 		  = this.upperHp.bind(this)
		this.delayGold 		  = this.delayGold.bind(this)
	}
	delaygold = React.createRef()
	upperhp = React.createRef()
	child = React.createRef(); // определяем реф для обращение доч. элемента к род. AllDamage
	childcolor = React.createRef(); // определяем реф для обращение доч. элемента к род. из компонента ListMonster
	Sum = (change) => { // функция запускающая дочернуюю фукцию компонента
		this.child.current.sumDamage()
	}
	Color = () => { // функция запускающая дочернуюю фукцию компонента
		this.childcolor.current.getColor()
	}
	upperHp(){ // Передаём эту функцию из компонента Юзер, что бы после передать её в компонент MAGAZINE
		this.upperhp.current.upperHp()
	}
	delayGold(){
		this.delaygold.current.delayGold()
	}
	componentWillMount(){ // по загрузке записываем в стейт значения
			this.state.monster.forEach((el,index) => {
				if(!(this.state.monsterdamage === el.damageAdd)){
				this.setState({monsterdamage:el.damageAdd})
				}
				if(!(this.state.hp === el.hp)){
					this.setState({
						hp:el.hp,
						startHP:el.hp
					})
				}	
			})
	}
	/*----------------------Передаю пропс из компонента GOLD сюда для изменения состояния ------------------*/
	goldBeforeDamage(change){
			this.setState({
				damage : this.state.damage * 2.5
			})
			let that = this
			setTimeout(function(){
				that.setState({
				damage: Math.ceil(that.state.damage / 2.5)
				})
			},10000)
	}
		/*----------------------Передаю вызов функции в  User для изменения состояния ------------------*/
	resetHp(change){
		this.setState({hp: this.state.startHP})
	}
	onChangeHp(){ // доделать функцию. Отнимаем заданное количетсво здоровья
		this.setState({hp:this.state.hp - 200})
	}
	/*----------------------------------------------------------------------------------*/
	/*----------------------Передаю пропс из компонента USER сюда для изменения состояния ------------------*/
/*	gameStart(change){
			this.setState({
				damage : this.state.damage * 2.5
			})
			let that = this
			setTimeout(function(){
				that.setState({
				damage: Math.ceil(that.state.damage / 2.5)
				})
			},10000)
	}*/
	/*----------------------------------------------------------------------------------*/
/*	getMonsterDamage(index){ 
		if(!(this.state.monsterdamage === this.state.monster[index].damageAdd)){
			this.setState({monsterdamage:this.state.monster[index].damageAdd})
		}
		if((this.state.hp > this.state.monster[index].hp) || (this.state.hp === this.state.monster[index].hp)){
			this.setState ({hp:this.state.monster[index].hp})
		}
		else{
			console.log(this.state.hp)
			console.log(this.state.monster[index].hp)
			console.log((this.state.hp === this.state.monster[index].hp))
			console.log('=')
			}
		} */
	addDamage(){
		this.setState({
			beforeDamage: 0
		})
		//С каждым кликом урон увеличивается на один
		if (this.props.showState !== true){
			this.setState({
				damage : this.state.damage + 1
			})
		} 
		// Шанс критического урона
		var double = Math.floor(Math.random() * Math.floor(3));
		if (double === 2){
			this.setState({
				beforeDamage : this.state.damage * 2,
			})
		}
		this.Sum(); // вызов доч.функции
		this.Color();
	}
	minusDamage(index){
		if(this.state.hp <= 0){
			this.setState({
				hp : 0
			})
		}
		else {
			this.state.beforeDamage === 0 
				? 
			this.setState({hp : this.state.hp - this.state.damage})
				:
			this.setState({hp: this.state.hp - this.state.beforeDamage})
		}
	}
	addDelay(){
			this.setState({showState:!this.state.showState})
			setTimeout(() => {
				this.setState({showState:!this.state.showState})
			},100)
			this.addDamage()
	}
	render(){
		const crit = (this.state.beforeDamage !== 0)
		? 
		<div className = "damage red">{this.state.beforeDamage}</div>
		: 
		<div className = "damage yellow">{this.state.damage}</div>

		const obj = this.state.monster.map((el,index) => {
		return(
			<div key = {index} className = "delay-damage">
			<div onClick = {this.minusDamage.bind(this,index)}  className = "u">
			<ListMonster 
			name 		 = {el.name}
			hp 			 = {this.state.hp}
			damage 		 = {this.state.damage}
			beforeDamage = {this.state.beforeDamage}
			ss 			 = {this.state.showState}
			ref 		 = {this.childcolor}
			/>
			<AllDamage 
			hp 			 = {this.state.hp}
			damage 		 = {this.state.damage}
			beforeDamage = {this.state.beforeDamage}
			ref 		 = {this.child} // определяем реф
			/>
			</div>
			<User
			damageAdd  	= {this.state.monsterdamage}
			resetHp   	= {this.resetHp}
			hp			= {this.state.hp}
			ref 		= {this.upperhp}
			delayGold 	= {this.delayGold}
			 />
			</div>
			)
		})
		const damage = this.state.showState ? '' : crit
		return(
			<div className = "main">
			<div className = "obj-npc" onClick = {this.addDelay.bind(this)}>
			{damage}
			{obj}
			</div>
			<Gold  
			beforeDamage 		= {this.state.beforeDamage}
			goldBeforeDamage 	= {this.goldBeforeDamage}
			onChangeHp 			= {this.onChangeHp}
			upperHp             = {this.upperHp}
			ref 				= {this.delaygold}
			/>
			</div>
			)
	}
}
class User extends Component{ // компонент Юзера
	constructor(){
		super()
		this.state = {
			resize:270,
			hpbar : 300,
			dead: false
		}
		this.upperHp = this.upperHp.bind(this)
	}
	upperHp(change){
		this.setState({hpbar: this.state.hpbar + 100})
	}
	youDead(){ // Метод отслеживающий состояние здоровья
		if(this.state.hpbar <= 0){
			console.log('1')
			this.setState({
				dead:true
			})
		}
	}
	startGame(){
		this.props.delayGold()
		this.damageToUser()
		this.props.resetHp()
		this.setState({
			hpbar:300,
			resize:270,
			dead: false
		})
	}
	damageToUser(){ // урон по нашему HP BAR
		let interval = setInterval(() => {
			if(this.state.hpbar <= 0){
				console.log('1')
				this.setState({
					dead:true
				})
			clearInterval(interval)
			}
			else {
				let hp = this.state.hpbar
				let damage = this.props.damageAdd
				let sum = hp - damage
				let resize = this.state.resize - 5
				this.setState({
					hpbar:sum,
					resize:resize
				})
			}
		},1000)

	}
	render(){
		const hpstyle = {width:this.state.resize + 'px'} // отнимаем немного пикселей для динамики
		const deadWindow = 
		<div className = "absolute-dead">
			<div className = "absolute-container">
				<span>Вы мертвы!</span>
				<button onClick = {this.startGame.bind(this)}>Начать заного</button>
			</div>
		</div>
		const live = 		
			<div className = "center">
					{/*<div className = "title">HP BAR</div>*/}
					<div style = {hpstyle} className = "user-hp">{this.state.hpbar}</div>
					<button className = "start-button" onClick = {this.startGame.bind(this)}>Начать игру</button>
			</div>
		const changeState = this.state.dead === false ? live : deadWindow
		return(
			<div>
			{changeState}
			</div>
			)
	}
}
class ListMonster extends Component{
	state = {
		sumDamage : 0,
		hpColor: ''
	}
	getColor(){
		let sum = this.state.sumDamage
		var percent = sum + this.props.hp // общее количество хп для определения процента
		var percentCount = sum / percent * 100 // вычесление процента от sum
			if(percentCount > 15 && percentCount < 40){
				this.setState({
					hpColor:'green'
				})
			}
			else if(percentCount > 40 && percentCount < 70){
				this.setState({
					hpColor:'yellow'
				})
			}
			else if (percentCount > 70) {
				this.setState({
					hpColor:'red'
				})		
			}

		this.props.beforeDamage === 0 
			?
		sum = sum + this.props.damage
			:
		sum = sum + this.props.beforeDamage
		this.setState ({
			sumDamage :  sum
		})
	}
		render(){
			const obj = 
			<div>
				<div className = "name" >{this.props.name}</div>

					{this.state.hpColor === 'green' 
						? 
					<div className = "whitegreen-hp">{this.props.hp}</div>
						:
					this.state.hpColor === 'yellow'
						?
					<div className = "yellow-hp">{this.props.hp}</div>
						:
					this.state.hpColor === 'red'  
						?
					<div className = "red-hp">{this.props.hp}</div>
						:
					<div className = "hp-bar">{this.props.hp}</div>
					}

				{this.props.ss ? <img src = {one} alt = "" /> : 
				<img src = {two} alt =""/>}
				{/*<img className = "blood-img" src = {blood} />*/}
			</div>
			return(
				<div>
					{obj}
				</div>
				)
		}
	
}
class AllDamage extends Component{ // компонент для подсчёта общего количесвта нанесёного урона
	state = {
		sumDamage : 0,
		startHP : 0
	}
	componentWillMount(){ 
		this.setState({startHP:this.props.hp})
	}
	sumDamage(){ // функция , которую передаём в родительский компонент
		let sum = this.state.sumDamage
		this.props.beforeDamage === 0 
			?
		sum = sum + this.props.damage
			:
		sum = sum + this.props.beforeDamage
		this.setState ({
			sumDamage : sum
		})
/*		if (this.state.sumDamage >= this.state.startHP){
			this.setState({sumDamage:0})
		}*/ // отчистка нанесёного урона.
	}
		render(){
			const obj = 
			<div>
				<div>
					<span>Количество нанесёного урона : </span>
					<span>{this.state.sumDamage}</span>
				</div>
			</div>
			return(
				<div className = "sidebar">
					{obj}
				</div>
				)
		}
	
}
class Gold extends Component{
	constructor(){
		super()
		this.state = {
			gold: 0,
			func: true,
			coinState:true,
			shopState:true,
			timer:60
		}
	this.onChangeHandler = this.onChangeHandler.bind(this)
	this.delayGold 		 = this.delayGold.bind(this)
	}
	componentDidMount(){
		this.setState({func:false})
	}	
	countDown(){ //таймер для отсчёта отката
		let that = this
		let timer = setInterval(function(){
			if(that.state.timer === 0 ) clearInterval(timer)
		that.setState({
			timer: that.state.timer -1
		})
		},1000)
		that.setState({
			timer:60
		})

	}
    onChangeHandler(change) {
    	this.setState({
    		shopState:false
    	})
    	this.countDown()
    	console.log(this.state.shopState)
    	let price = 5
    	let gold = this.state.gold
    	let sum = 0
    	if(gold >= price){
    		sum = gold - price
	    	this.setState({
	    		gold: sum
	    	})
	    	this.props.goldBeforeDamage()
    	}
    	else{console.log('poka')}
    	let that = this
   		setTimeout(function(){
			that.setState({
			shopState: true
			})
		},60000)
    }

	delayGold(change){
		this.setState({
			gold:0
		})
		let gold = 0
		setInterval(() => {
			gold = this.state.gold + 1
			this.setState({
				gold:gold,
				coinState:!this.state.coinState
			})
		},2000)
	}

	render(){
		/*this.state.func === true ? this.delayGold() : console.log()*/
		/*this.state.func = false*/
		let hide = this.state.coinState === true ? <span className = "hide-coin">+1</span> : ''
		return(
			<div className = "container">
				<div className = "gold-container">
					{hide}
					<div className = "coin">
						<img src = {coin} alt="pot of gold"/>
					</div>
					<div className = "pot">
						<img src = {pot} alt="pot of gold"/>
					</div>
					<div className = "gold-count">{this.state.gold}</div>
				
				</div>
				<Magazine 
				gold  			= {this.state.gold}
				shopState 		= {this.state.shopState}
				timer 			= {this.state.timer}
				beforeDamage 	= {this.props.beforeDamage}
				onChangeHandler = {this.onChangeHandler}
				onChangeHp    	= {this.props.onChangeHp}
				upperHp         = {this.props.upperHp}
				/>
			</div>
			)
	}
}
class Magazine extends Component{

	Criptomaniac(){
		this.props.onChangeHandler()
	}
	hustleBird(){
		this.props.onChangeHp()
	}
	upperBird(){
		this.props.upperHp()
	}

	render(){
		var okey = <button onClick = {this.Criptomaniac.bind(this)} className = "action">Купить</button>
		let cancel = <span className = "action">{this.props.timer}</span>
		return(
			<div className = "mag-menu">
			<span>Barber Shop</span>
				<div className = "section">
					<h3>CRITOMANIAC</h3>
					<article>Ты прошёл долгий путь войн...Твоя борода нуждается в 250% умножении урона на 10 секунд за бесценок!</article>
					{this.props.shopState === true ? okey : cancel}
				</div>
				<div className = "section">
					<h3>BEARD HUSTLE</h3>
					<article>Прерасный пируэт с разворотом на 180 градусов задаёт невероятный импульс движения твоей бороды. Да она словно хлыст! -200 здоровья</article>
					<button onClick = {this.hustleBird.bind(this)} className = "action">Купить</button>
				</div>
				<div className = "section">
					<h3>PRAISE THE BIRD</h3>
					<article>Аномальный рост бороды! +100 к здоровью</article>
					<button onClick = {this.upperBird.bind(this)} className = "action">Купить</button>
				</div>
				<div className = "section">
					<h3>GOLDEATER</h3>
					<article>Каждый удар будет стоить 30 золотых в течении 20 секунд. Твоя мощь с каждым кликом возрастает в полтора раза!Заряди ему монетой!</article>
					<button onClick = {this.Criptomaniac.bind(this)} className = "action">Купить</button>
				</div>
				<div className = "section">
					<h3>THE GOD OF THE BEARD</h3>
					<article>Твоя борода окутывает твое тело создавая непроницаемый щит. Невоспреимчивость к физическим атакам в течении 20 секунд!</article>
					<button onClick = {this.Criptomaniac.bind(this)} className = "action">Купить</button>
				</div>
			</div>
			)
	}
}

ReactDOM.render(<App />, document.getElementById('root') )

