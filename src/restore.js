class App extends Component{
  constructor(){
    super()
    this.state = { 
      name:'',
      surname:'',
      comment:'',
      value:'',
      isOpen:true,
      people: [
        {
        name:"Dima",
        surname:"kaplenko",
        comment: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",
        like: 0,
        id:11111,
        visible:true
        },
       {
        name:"Vanya",
        surname:"Zenko",
        comment: " The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.",
        like: 0,
        id:22222,
        visible:true
        },
                {
        name:"Kolo",
        surname:"Krate",
        comment: " It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
        id:33333,
        like: 0,
        visible:true
        },
                {
        name:"Raoden",
        surname:"Raoden",
        comment: " It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
        id:444444,
        like: 0,
        visible:true
        }
      ]
    }
  }
 addName(event){
    this.setState({
    name:event.target.value
    })
  }
 addSurname(event){
    this.setState({
    surname:event.target.value
    })
  }
 addText(event){
    this.setState({
    comment:event.target.value
    })
  }
  Comment(){
    const obj = {
      name: this.state.name,
      surname:this.state.surname,
      comment: this.state.comment,
      like: 0,
      visible:true
    }
    this.state.people.push(obj)
    this.setState({name:'', surname: '', comment:''})
    this.setState({people:this.state.people})
  }

      deleteItem(index){
      this.state.people.splice(index, 1);
      this.setState({people : this.state.people})
  }
      addLike(index){
        let like = this.state.people[index].like;
        like = 1 + like;
        this.state.people[index].like = like
        console.log(this.state.people)
        this.setState({people: this.state.people})
      }
      changeVisible(index){
        this.state.people[index].visible = !this.state.people[index].visible
        this.setState({people:this.state.people})
      }
      changeText(index, event){
        let text = event.target.value
        this.setState({value: event.target.value})
        this.state.people[index].comment = text;
      }
      renderComment(){
        this.setState({people:this.state.people})
      }
  render(){
    const flex = {display:'flex', flexDirection:'column', alignItems:'center'}
    const margin = {marginRight:'5px'}
    const db = {display:'block', width:'60vw'}
    const width = {width:'60vw'}
    const float = {float:'right'}
    const textcenter = {textAlign:'center'}
    const obj = this.state.people.map((item, index) => {
        return (
         <div className = "comment" key = {index}>
            <div className = "comment-title" style = {textcenter}>
              <span className = "comment-title__name" style = {margin}>{item.name}</span>
              <span className = "comment-title__surname" >{item.surname}</span>
            </div>
           {item.visible ===  true  ? 
              <div   className = "comment-text" style = {width}>{item.comment}</div> : 
              <textarea type = "text" value = {item.comment} onChange = {this.changeText.bind(this, index)} rows = "5" style = {db}></textarea>}

            
            <span className = "comment-like" style = {float}>{item.like}</span>
            <button className = "comment-button" onClick = {this.changeVisible.bind(this, index)}>Change</button>
            <button className = "comment-button" onClick ={this.deleteItem.bind(this, index)}>
              Удалить комментарий
            </button>
            <button className = "comment-button-like" style = {float} onClick = {this.addLike.bind(this, index)}>&#9825;</button>
        </div>
        )
        
    })
    return(
      <div className = "wrapper" style = {flex} >
      {obj}
      <div className = "comment-add">
        <input className = "comment-add-input" value = {this.state.name} onChange = {this.addName.bind(this)}type = "text"></input>
        <input  className = "comment-add-input" value = {this.state.surname} onChange = {this.addSurname.bind(this)} type = "text"></input>
        <textarea  className = "comment-add-area" value = {this.state.comment} onChange = {this.addText.bind(this)} ></textarea>
        <button className = "comment-add-button comment-button" onClick = {this.Comment.bind(this)}>Submit</button>
      </div>
     {/*<AddComent people = {this.state.people} Comment = {this.renderComment}/>*/}
      </div>
      )
  }
}

render(<App />,
  document.getElementById('root')
  )